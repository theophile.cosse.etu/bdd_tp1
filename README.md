# bdd_tp1

Théophile Cosse

IP de la VM : 172.28.100.31

Accès au serveur ssh cha@172.28.100.31 mot de passe cha

# Q8
La base de donnée microblog permet la création d'utilisateur et de message, de follow d'autre utilisateur et d'afficher son feed.

# Q9

Est-ce qu’il est possible pour l’utilisateur common_user:
* de récupérer la totalité des messages publiés : Uniquement les 30 dernier messages.
* de récupérer la liste des utilisateurs inscrits : Non
* de récupérer le mot de passe d’un utilisateur inscrit : Non

Est-il possible pour l’utilisateur possédant la base de données

* de récupérer la totalité des messages publiés : Oui
* de récupérer la liste des utilisateurs inscrits : Oui
* de récupérer le mot de passe d’un utilisateur inscrit : Oui mais les mdp sont crypté

# Q10
La commande `CLUSTER` réarrange l'ordre ( physique ) selon lequel sont rangées les tuples en fonction de l'index spécifié.


La ligne ci-dessous sers donc a ranger les message par ordre de publication.

```CLUSTER messages_store USING idx_pub_date;```


# Benchmarking

Résultat du benchmark :

VM taille "petite"
* 1 processus = ~23 TPS
* 2 processus = ~42 TPS
* 8 processus = ~120 TPS
* 16 processus = ~150 TPS
* 32 processus = ~180 TPS
* 100 processus = ~crash TPS
* 256 processus = ---

Impossible d'attribuer plus de ressource sur openstack

On peut donc imaginer que postgres tournant sur un VM aux capacités limité peut servir de base de données pour un site web recevant plusieurs milliers d'utilisateurs par jour. Même s'il est très difficile d'émettre une approximation puisque tout dépant du pique de fréquentation, si 200 utilisateurs accede a au site la même seconde la base de données ne tiendra surement pas.

# Amélioration possible / limitation

Par souci de simplicité (évité d'avoir à stocker les uuid de chaque utilisateur) j'ai choisi d'utiliser un pool d'utilisateur propre à chaque processus.
Cela ne devrait en principe pas affecter le résultat des tests puisque ici le facteur définissant le nombre de transaction par seconde, les requête n'étant pas effectué de manière asynchrone, est le nombre de requête lancer simultanément.

On aurait potentiellement pu éviter de devoir creer 32 processus en utilisant des requêtes asynchrones (non bloquantes). 




