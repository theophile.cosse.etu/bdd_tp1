#!/bin/bash

echo "emptying database"
psql -h 172.28.100.31 -U tc -d microblog -c "truncate user_store CASCADE"
echo "db empty"

echo "starting $1 proc";
for i in $(seq 1 $1); do
    echo "start proc test_$1_$i"
    python test.py "$1 $i test" &
done
wait

