import psycopg2
from collections import namedtuple
from time import time

Message = namedtuple("Message", ("message_id", "content", "date", "replies_to", "user_name", "user_id"))


def logged(f):
	def nf(self, *args, **kwargs):
		t0 = time()
		r = f(self, *args, **kwargs)
		self.acc_time += time() - t0
		self.nb_query += 1
		return r
	return nf


class microblog:
	def __init__(self, ip,user_prefix,nb_user,nb_op):
		self.db = psycopg2.connect(host=ip, database="microblog", user="common_user")
		self.acc_time = 0
		self.nb_query = 0
		self.nb_user = nb_user
		self.user_prefix = user_prefix
		self.uuid_table = []
		self.nb_op = nb_op
		for i in range(nb_user):
			self.uuid_table.append(self.create_user(i))

	def start_test(self):
		for i in range(self.nb_user):
			index_str = str(i)
			self.follow(self.user_prefix + index_str,index_str,self.uuid_table[(i+1)%self.nb_user])
			self.follow(self.user_prefix + index_str,index_str,self.uuid_table[(i+2)%self.nb_user])

			for j in range(self.nb_op):
				self.publish_post(self.user_prefix + index_str,index_str,index_str)
				self.get_feed(self.user_prefix + index_str,index_str)
				self.get_messages()


	def create_user(self,user_index):
		""""
			create an user
		"""
		with self.db:
			with self.db.cursor() as curs:
				curs.execute("SELECT create_user(%s, %s)", (self.user_prefix + str(user_index), str(user_index)))
				return curs.fetchone()[0]
	@logged
	def publish_post(self, user, password, content, mid=None):
		""""
			publish a post for user "user". If mid is set to some message id, will act as a reply to.
		"""
		with self.db:
			with self.db.cursor() as curs:
				curs.execute("SELECT insert_message(%s, %s, %s, %s)", (user, password, content, mid))
				return curs.fetchone()[0]

	@logged
	def follow(self, user_id, password, other_id):
		"""
			Make user_id follow a other_id
		"""
		with self.db:
			with self.db.cursor() as curs:
				curs.execute("SELECT follow(%s, %s, %s)", (user_id, password, other_id))

	@logged
	def get_feed(self, user, password, limit=50):
		"""
			Get the 50 fist message of the feed of user
		"""
		with self.db:
			with self.db.cursor() as curs:
				curs.execute("SELECT * FROM feed(%s,%s) LIMIT %s", (user, password, limit))
				return curs.fetchall()

	@logged
	def get_messages(self):
		"""
			Get last posted message
		"""
		with self.db:
			with self.db.cursor() as curs:
				curs.execute("SELECT * FROM messages")
				return curs.fetchall()
